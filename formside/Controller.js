var Controller = (function(){	
			function Controller(){	

				var self = this;
				
				this.e = {};
				this.__id = {};
				this.__name = {};
				this.__attribute = {};
				this.__class = {};
				this.__type = {};

				this.__construct = function(container){
					
				}
						
				this.statusPanel = false;

				this.__type.form = function(element){

					if( self.attr.action != null){
						element.setAttribute('action',self.attr.action);
					}


					if(self.attr.defaultinputs === 'false'){						
						element.querySelector('.defaultinputs').innerHTML = '';
					}

					return {
						submit:function(event){

							var url = self.attr.ajax || null;
								
							if(url == null)return true;

							element.querySelector('.response').classList.remove('error');
							element.querySelector('.response').classList.remove('success');
							element.querySelector('.response').innerHTML = '';
							element.querySelector('[type="submit"]').classList.add('loading');

							

							var msgsuccess = self.attr.msgsuccess || 'Mensagem enviada com sucesso';
							var msgerror =  self.attr.msgerror || 'Falha ao enviar a mensagem'
							var redirect =  self.attr.redirect || null;

							var formData = {};
							for(key in element.elements){
								var name = element.elements[key].name;
								var value = element.elements[key].value;

								if(value !== undefined)
								formData[name] = value;
							}
							

							raiz.essentials.ajax({
								url:url,
								method:'post',
								data:formData,
								success:function(response){
									if(raiz.essentials.checkJson(response)){
										response = JSON.parse(response);
										if(response.status == 'success'){											
											element.querySelector('[type="submit"]').classList.remove('loading');
											element.querySelector('.response').classList.add('success');
											element.querySelector('.response').innerHTML = msgsuccess;

											setTimeout(function(redirect){
												if(redirect != null) window.location = redirect;
											},1000,redirect);

										}
									}else{

										

										element.querySelector('[type="submit"]').classList.remove('loading');
											element.querySelector('.response').classList.add('error');
											element.querySelector('.response').innerHTML = msgerror;
										return false;
									}
								}
							});


							return false;
						}
					}
				}

				this.e.togglePanel = function(element){

					return {
						click:function(event){
							
							if(self.statusPanel == true){
								self.statusPanel = false;
								self.component.classList.remove('active');
							}else{
								self.statusPanel = true;								
								self.component.classList.add('active');
							}
						}
					}
				}


				return this;
			}

			return new Controller;
		})();
